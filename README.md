# Verotel flex pay (single payment) for membership 2 plugin  

Implementation of legacy membership and Verotel flex pay

See:
* Verotel purchase [docs](https://controlcenter.verotel.com/flexpay-doc/purchase.html)  
* Legacy membership [repo](https://github.com/wpmudev/membership-2)   