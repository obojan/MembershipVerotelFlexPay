<?php

namespace App\Service\MembershipVerotelFlexPay;

use Verotel\FlexPay\Brand;
use Verotel\FlexPay\Client as FlexPayClient;
use Verotel\FlexPay\Exception as FlexPayException;

/**
 * Verotel flex pay (single payment) for membership 2 plugin
 *
 * https://controlcenter.verotel.com/flexpay-doc/purchase.html
 * https://github.com/wpmudev/membership-2
 */
class FlexPayClientBridge implements FlexPayClientBridgeInterface
{
    private int $shopId;
    private string $merchantId;
    private FlexPayClient $flexPayClient;

    /**
     * @param int $shopId
     * @param string $merchantId
     * @param string $secret
     * @throws FlexPayException
     */
    public function __construct(int $shopId, string $merchantId, string $secret)
    {
        $this->shopId = $shopId;
        $this->merchantId = $merchantId;
        $this->flexPayClient = $this->buildFlexPayClient($secret);
    }

    /**
     * @param string $secret
     * @return FlexPayClient
     * @throws FlexPayException
     */
    private function buildFlexPayClient(string $secret): FlexPayClient
    {
        return new FlexPayClient(
            $this->shopId,
            $secret,
            Brand::create_from_merchant_id($this->merchantId)
        );
    }

    public function getBayUrl(float $priceAmount, string $priceCurrency, string $description, int $invoiceId): string
    {
        return $this->flexPayClient->get_purchase_URL(
            [
                "priceAmount" => $priceAmount,
                "priceCurrency" => $priceCurrency,
                "description" => $description,
                "custom1" => $invoiceId,
            ]
        );
    }

    // Validate data that Verotel sent within request to postback page
    public function isValidPostBackRequest(array $request): bool
    {
        if (isset($request[MS_Gateway_Flexpay::POSTBACK_PARAM_NAME])) {
            unset($request[MS_Gateway_Flexpay::POSTBACK_PARAM_NAME]);
        }
        return $this->flexPayClient->validate_signature($request);
    }

    // Validate data that Verotel sent within request to success page
    public function isValidDataReceivedOnSuccessPage($receivedData): bool
    {
        if (!$this->successRequestBasicValidation($receivedData)) {
            return false;
        }
        $saleId = $receivedData['saleID'];
        $paramsForStatusCheck = array(
            'version' => FlexPayClient::PROTOCOL_VERSION,
            'shopID' => $this->shopId,
            'saleID' => $saleId,
        );
        $statusUrl = $this->flexPayClient->get_status_URL($paramsForStatusCheck);
        return $this->isStatusApproved($statusUrl, $saleId);
    }

    // Fetches order status from Verotel FlexPay
    private function isStatusApproved(string $statusUrl, string $saleId): bool
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $statusUrl);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);//true on prod
        $responseBody = curl_exec($ch);
        curl_close($ch);

        if (empty($responseBody)) {
            return false;
        }

        $checks = array(
            0 => 'response: FOUND',
            1 => 'saleResult: APPROVED',
            2 => 'saleID: ' . $saleId,
        );
        foreach ($checks as $check) {
            if (strpos($responseBody, $check) === false) {
                return false;
            }
        }
        return true;
    }

    private function successRequestBasicValidation($receivedData): bool
    {
        $saleId = (!empty($receivedData['saleID']) && is_scalar($receivedData['saleID'])) ? $receivedData['saleID'] : null;
        $hasSignature = !empty($receivedData['signature']);
        $hasInternalInvoiceId = !empty($receivedData['custom1']);//membership plugin invoice id

        if (isset($receivedData[MS_Gateway_Flexpay::SUCCESS_PARAM_NAME])) {
            ///////////////// only for develop/////////////////////////
            // unset($receivedData[MS_Gateway_Flexpay::SUCCESS_PARAM_NAME]);
        }

        if (
            empty($saleId) ||
            !$hasSignature ||
            !$hasInternalInvoiceId ||
            !$this->flexPayClient->validate_signature($receivedData)
        ) {
            return false;
        }
        return true;
    }
}