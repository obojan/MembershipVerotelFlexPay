<?php

namespace App\Service\MembershipVerotelFlexPay;

interface FlexPayClientBridgeInterface
{
    public function getBayUrl(float $priceAmount, string $priceCurrency, string $description, int $invoiceId): string;

    public function isValidPostBackRequest(array $request): bool;

    public function isValidDataReceivedOnSuccessPage($receivedData): bool;
}